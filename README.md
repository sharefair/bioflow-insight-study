# BioFlow-Insight-Study

In this repository is presented a notebook to reproduce the analysis of a large dataset of workflows.

To run the analysis simply, clone the repository by running this command :

`git clone --recurse-submodules https://gitlab.liris.cnrs.fr/sharefair/bioflow-insight-study.git`

Then simply execute the Python notebook.

> `unzip` might have to be installed. But it can be replaced by an equivalent tool.

> Note : To install graphviz, in linux you might need to execute this command `sudo apt install graphviz`

<!--`git submodule update --recursive --remote`-->

